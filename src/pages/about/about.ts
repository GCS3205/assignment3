import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  res:any;
  uname:any
  login:any = {};
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.res = navParams.get("val");
    this.uname = navParams.get("uname");
  }

  logForm(){
    let uname = this.login['uname'];
    let password = this.login['pass'];
    alert("Your username is"+uname+" and password is "+password);
    // https
  }
}
